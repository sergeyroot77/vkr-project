# Разработка системы стилизации изображения на основе подхода Deep Photo Style Transfer

---

Используется Python 3.7
Используемые библиотеки:

```python
absl-py==1.2.0
appdirs==1.4.4
astor==0.8.1
autopep8==2.0.2
cycler==0.11.0
fonttools==4.38.0
gast==0.5.3
google-pasta==0.2.0
grpcio==1.49.1
h5py==3.7.0
importlib-metadata==5.0.0
joblib==1.2.0
keras==2.10.0
Keras-Applications==1.0.8
Keras-Preprocessing==1.1.2
kiwisolver==1.4.4
Mako==1.2.3
Markdown==3.4.1
MarkupSafe==2.1.1
matplotlib==3.5.3
numpy==1.16.4
packaging==21.3
pandas==1.1.5
Pillow==9.3.0
pip==22.2.2
platformdirs==2.5.2
protobuf==3.20.3
pycodestyle==2.10.0
pycuda==2021.1
pyparsing==3.0.9
python-dateutil==2.8.2
pytools==2022.1.12
pytz==2022.6
scikit-learn==1.0.2
```

Установить библиотеки можно командой `pip install -r requirements.txt`

Дополнительная утилиты: Cuda 2021

## Реализация функции Лаплассиана

Реализовано в файле `closed_from_matting.py`

```python
def getlaplacian1(i_arr, consts, epsilon=1e-5, win_rad=1):
    neb_size = (win_rad * 2 + 1) ** 2
    h, w, c = i_arr.shape
    img_size = w * h
    consts = spi.morphology.grey_erosion(consts, footprint=np.ones(shape=(win_rad * 2 + 1, win_rad * 2 + 1)))
    indsM = np.reshape(np.array(range(img_size)), newshape=(h, w), order='F')
    tlen = int((-consts[win_rad:-win_rad, win_rad:-win_rad] + 1).sum() * (neb_size ** 2))
    row_inds = np.zeros(tlen)
    col_inds = np.zeros(tlen)
    vals = np.zeros(tlen)
    l = 0
    for j in range(win_rad, w - win_rad):
        for i in range(win_rad, h - win_rad):
            if consts[i, j]:
                continue
            win_inds = indsM[i - win_rad:i + win_rad + 1, j - win_rad: j + win_rad + 1]
            win_inds = win_inds.ravel(order='F')
            win_i = i_arr[i - win_rad:i + win_rad + 1, j - win_rad: j + win_rad + 1, :]
            win_i = win_i.reshape((neb_size, c), order='F')
            win_mu = np.mean(win_i, axis=0).reshape(c, 1)
            win_var = np.linalg.inv(
                np.matmul(win_i.T, win_i) / neb_size - np.matmul(win_mu, win_mu.T) + epsilon / neb_size * np.identity(

                    c))
            win_i2 = win_i - np.repeat(win_mu.transpose(), neb_size, 0)
            tvals = (1 + np.matmul(np.matmul(win_i2, win_var), win_i2.T)) / neb_size
            ind_mat = np.broadcast_to(win_inds, (neb_size, neb_size))
            row_inds[l: (neb_size ** 2 + l)] = ind_mat.ravel(order='C')
            col_inds[l: neb_size ** 2 + l] = ind_mat.ravel(order='F')
            vals[l: neb_size ** 2 + l] = tvals.ravel(order='F')
            l += neb_size ** 2
    vals = vals.ravel(order='F')[0: l]
    row_inds = row_inds.ravel(order='F')[0: l]
    col_inds = col_inds.ravel(order='F')[0: l]
    a_sparse = sps.csr_matrix((vals, (row_inds, col_inds)), shape=(img_size, img_size))
    sum_a = a_sparse.sum(axis=1).T.tolist()[0]
    a_sparse = sps.diags([sum_a], [0], shape=(img_size, img_size)) - a_sparse
    return a_sparse
```

Данный код предназначен для вычисления Лапласиана (дифференцирующей) матрицы для обработки изображений. Он представляет собой реализацию метода "Laplacian pyramid image blending" для соединения двух изображений

У функции getlaplacian1() есть несколько параметров:

- `i_arr` – массив значений пикселей изображения, для которого нужно вычислить Лапласиан матрицу;
- `consts` – массив, содержащий информацию о пикселях, которые не нужно учитывать при обработке;
- `epsilon` – значение для обработки матриц;
- `win_rad` – целочисленное значение для радиуса вычисления дифференцирующей матрицы.

Первая строка функции определяет размерности ядра, используемого при сканировании матрицы.

Затем определяются размерности изображения – его высота `h`, ширина `w` и количество цветовых каналов `c`. Далее вычисляется размерность всей матрицы (всех пикселей), путем перемножения ширины и высоты.

Затем происходит морфологическая обработка массива `consts`, который содержит информацию о пикселях, которые не нужно учитывать при формировании Лапласиана. В данной реализации используется эрозия для удаления из этого массива пикселей с помощью функции `gray_erosion` из библиотеки SciPy. После этого определяются индексы матрицы `indsM`, которые соответствуют соответствующим пикселям в изображении.

Далее вычисляется размерность массивов, которые будут содержать индексы, линейную отображенность и значения дифференцирующей матрицы. Эти массивы затем используются для создания разреженной Лапласиан матрицы.

Цикл, начинающийся со строки `for j in range(win_rad, w - win_rad):`, сканирует все пиксели изображения, начиная с `win_rad` и заканчивая `w - win_rad` для осей `x` и `y` соответственно. Если пиксель является константной - определенной в массиве consts – то цикл пропускается. Если пиксель не является константным, этот пиксель и его соседние пиксели в определенном окне вычисляются и преобразуются в вектор. Затем этот вектор усредняется, и дисперсия вычисляется для этого вектора. Следующим шагом вычисляются значения дифференцирующей матрицы на основе полученных векторов. Индексы, значения и линейную отображенность дифференцирующей матрицы затем добавляются в соответствующие массивы.

После этого создается разреженная матрица `a_sparse`, используя массивы индексов, линейности и значений. В конце функции определяется размерность и суммируются элементы матрицы Лапласиана по строке. Это используется для создания матрицы `L`, которая является разницей между диагональной матрицей, представленной значениями матрицы Лапласиана, и разреженной матрицой, возвращенной функцией `getlaplacian1()`.

```python
def getLaplacian(img):
    h, w, _ = img.shape
    coo = getlaplacian1(img, np.zeros(shape=(h, w)), 1e-5, 1).tocoo()
    indices = np.mat([coo.row, coo.col]).transpose()
    return tf.SparseTensor(indices, coo.data, coo.shape)
```

Функция `getLaplacian()` – это обертка для `getlaplacian1()`, которая преобразует результат функции `getlaplacian1() `в разреженный тензор `TensorFlow SparseTensor.` Для этого она принимает на вход вычисленный Лапласиан в виде `scipy.sparse.coo_matrix` и преобразует его в TensorFlow SparseTensor через `np.mat` и `tf.SparseTensor`

## Реализация Smooth Local Affine

Реализовано в файле `smooth_local_affine.py`

Используется для улучшения качества изображений путем сглаживания локальных аффинных трансформаций на патчах изображения.

Функция принимает на вход следующие аргументы:

- `output_`: массив `numpy` с выходными данными (улучшенное изображение)
- `input_`: массив `numpy` с входными данными (исходное изображение)
- `epsilon`: малая константа используемая в вычислениях
- `patch`: размер патча
- `h` и `w`: высота и ширина входного изображения
- `f_r` и `f_e`: дополнительные параметры для настройки алгоритма

Первым шагом функция создает экземпляр класса SourceModule из библиотеки `pycuda.compiler`, позволяющий компилировать код на языке CUDA.

Затем функция переводит входные и выходные данные в формат, понимаемый языком CUDA, с помощью функции `cuda.mem_alloc`.

Далее происходит вызов ядра kernel_smooth_local_affine, которое является функцией, выполняющей аффинные преобразования на каждом патче изображения и сглаживающей полученные результаты. Задача ядра – это применить аффинные преобразования к каждому патчу изображения, а также вычислить матрицу локального сглаживания на каждом патче.

На каждой итерации подготавливается матрица локальной аффинной трансформации на патче. В цикле перебираются все пиксели на изображении с шагом ` patch / 2`. Для каждого пикселя производится вычисление матрицы трансформации с помощью функции `calc_transform_matrix`, которая принимает в качестве аргументов координаты патча и настраиваемые параметры `f_r`, `f_e`. Затем происходят вычисления аффинного преобразования для текущего патча, используя вычисленную на предыдущем шаге матрицу трансформации.

После вычисления всех аффинных преобразований происходят вычисления матрицы локального сглаживания. В каждом патче строится ковариационная матрица (матрица ковариации), которая далее используется для вычисления инвертированной матрицы и коэффициентов локального сглаживания. Затем происходит сглаживание изображения на каждом патче с помощью вычисленных коэффициентов локального сглаживания.

В конце всех итераций данные возвращаются в формат `numpy` и записываются в массив `output_`. Возвращает функция ничего.

В целом, функция реализует алгоритм сборки изображения из частей, сглаживая локальные аффинные трансформации на каждом патче изображения. Алгоритм является достаточно сложным и задействует множество математических вычислений.

## Реализация функции преобразования в RGB и BGR.

Реализовано в файле `photo_style.py`

```python
VGG_MEAN = [103.939, 116.779, 123.68]
```

Константа VGG_MEAN задает среднее значение по каждому каналу в изображениях, используемых для обучения нейронной сети VGG. Данное среднее значение используется для нормализации изображения в функциях `rgb2bgr` и `bgr2rgb`

```python
def rgb2bgr(rgb, vgg_mean=True):
    if vgg_mean:
        return rgb[:, :, ::-1] - VGG_MEAN
    else:
        return rgb[:, :, ::-1]
```

Функция `rgb2bgr` принимает в качестве аргумента исходное изображение в формате RGB и параметр vgg_mean, `который` по умолчанию равен `True`. Если значение параметра `vgg_mean` равно `True`, то к каждому каналу исходного изображения применяется операция вычитания соответствующего значения из константы `VGG_MEAN`. Затем каналы меняют местами для преобразования изображения из формата RGB в формат BGR.

```python
def bgr2rgb(bgr, vgg_mean=False):
    if vgg_mean:
        return bgr[:, :, ::-1] + VGG_MEAN
    else:
        return bgr[:, :, ::-1]
```

Функция `bgr2rgb` принимает в качестве аргумента изображение в формате BGR и параметр `vgg_mean`, который по умолчанию равен `False`. Если значение параметра `vgg_mean` равно `True`, то к каждому каналу применяется операция прибавления соответствующего значения из константы `VGG_MEAN`. Затем каналы меняют местами для преобразования изображения из формата BGR в формат RGB.

## Реализация матрицы Грама

Реализовано в файле `photo_style.py`

```python
def gram_matrix(activations):
    height = tf.shape(activations)[1]
    width = tf.shape(activations)[2]
    num_channels = tf.shape(activations)[3]
    gram_matrix = tf.transpose(activations, [0, 3, 1, 2])
    gram_matrix = tf.reshape(gram_matrix, [num_channels, width * height])
    gram_matrix = tf.matmul(gram_matrix, gram_matrix, transpose_b=True)
    return gram_matrix
```

Входным аргументом функции являются активации, передаваемые в качестве тензора TensorFlow. Активации могут быть получены, например, путем прогонки изображения через сверточную нейронную сеть. Активации содержат промежуточные значения, которые представляют выходы слоев сети на заданных входных данных.

Для вычисления матрицы Грама функция сначала определяет размеры тензора активаций. Затем выполнится транспонирование тензора таким образом, чтобы его последний индекс стал первым (например, в развернутом виде это означает, что оси, соответствующие высоте и ширине, будут транспонированы). Затем тензорakteria преобразуется в двумерную матрицу путем слияния измерений, соответствующих высоте и ширине, после чего выполняется умножение матрицы на ее транспонированную копию.

В результате выполнения функции gram_matrix возвращает матрицу Грама, которая представляет собой квадратную матрицу размером `num_channels` x `num_channels`, где `num_channels` представляет количество фильтров в заданном тензоре активаций. Каждый элемент матрицы Грама отражает корреляцию между активациями на всех позициях сверточного фильтра с одним из сверточных фильтров. Таким образом, матрица Грама может быть использована в качестве меры стиля в стилевом переносе изображений.

## Реализация функций потерь

Реализовано в файле `photo_style.py`

### Потери контента и стиля

```python
def content_loss(const_layer, var_layer, weight):
    return tf.reduce_mean(tf.squared_difference(const_layer, var_layer)) * weight

  
  

def style_loss(CNN_structure, const_layers, var_layers, content_segs, style_segs, weight):
    loss_styles = []
    layer_count = float(len(const_layers))
    layer_index = 0
    _, content_seg_height, content_seg_width, _ = content_segs[0].get_shape().as_list()
    _, style_seg_height, style_seg_width, _ = style_segs[0].get_shape().as_list()
    for layer_name in CNN_structure:
        layer_name = layer_name[layer_name.find("/") + 1:]
        
        if "pool" in layer_name:
            content_seg_width, content_seg_height = int(math.ceil(content_seg_width / 2)), int(
                math.ceil(content_seg_height / 2))
            style_seg_width, style_seg_height = int(math.ceil(style_seg_width / 2)), int(
                math.ceil(style_seg_height / 2))

            for i in xrange(len(content_segs)):
                content_segs[i] = tf.image.resize_bilinear(content_segs[i],
                                                          tf.constant((content_seg_height, content_seg_width)))
                style_segs[i] = tf.image.resize_bilinear(style_segs[i],
                                                         tf.constant((style_seg_height, style_seg_width)))

        elif "conv" in layer_name:
            for i in xrange(len(content_segs)):
              
                content_segs[i] = tf.nn.avg_pool(tf.pad(content_segs[i], [[0, 0], [1, 1], [1, 1], [0, 0]], "CONSTANT"),

                                                 ksize=[1, 3, 3, 1], strides=[1, 1, 1, 1], padding='VALID')

                style_segs[i] = tf.nn.avg_pool(tf.pad(style_segs[i], [[0, 0], [1, 1], [1, 1], [0, 0]], "CONSTANT"),

                                               ksize=[1, 3, 3, 1], strides=[1, 1, 1, 1], padding='VALID')


        if layer_name == var_layers[layer_index].name[var_layers[layer_index].name.find("/") + 1:]:
            print("Setting up style layer: <{}>".format(layer_name))
            const_layer = const_layers[layer_index]
            var_layer = var_layers[layer_index]
            layer_index = layer_index + 1
            layer_style_loss = 0.0
            for content_seg, style_seg in zip(content_segs, style_segs):
                gram_matrix_const = gram_matrix(
                    tf.multiply(const_layer, style_seg))
                style_mask_mean = tf.reduce_mean(style_seg)
                gram_matrix_const = tf.cond(tf.greater(style_mask_mean, 0.),

                                            lambda: gram_matrix_const / (

                    tf.to_float(tf.size(const_layer)) * style_mask_mean),

                    lambda: gram_matrix_const

                )
                gram_matrix_var = gram_matrix(
                    tf.multiply(var_layer, content_seg))
                content_mask_mean = tf.reduce_mean(content_seg)
                gram_matrix_var = tf.cond(tf.greater(content_mask_mean, 0.),
                                          lambda: gram_matrix_var / (

                    tf.to_float(tf.size(var_layer)) * content_mask_mean),
                    lambda: gram_matrix_var

                )
                diff_style_sum = tf.reduce_mean(
                    tf.squared_difference(gram_matrix_const, gram_matrix_var)) * content_mask_mean
                layer_style_loss += diff_style_sum
            loss_styles.append(layer_style_loss * weight)

    return loss_styles
```

Данный код содержит две функции, которые используются для вычисления потерь при создании изображения в стиле и контенте других изображений с помощью нейронной сети.

Функция `content_loss(const_layer, var_layer, weight)` вычисляет потерю контента. Входные параметры - константный слой `(const_layer)`, переменный слой `(var_layer)` и вес `(weight)`. С помощью tensorflow функция вычисляет среднеквадратичную разность между значениями `const_layer` и `var_layer`, а затем `умножает` результат на вес.

Функция `style_loss(CNN_structure, const_layers, var_layers, content_segs, style_segs, weight)` вычисляет потери стиля. Входные параметры – структура 38 нейронной сети `(CNN_structure)`, константные слои `(const_layers)`, переменные слои `(var_layers)`, сегменты контента ` (content_segs)`, сегменты стиля ` (style_segs)` и вес `(weight)`.

Функция проходит по всем слоям `CNN_structure` и определяет, является ли текущий слой константным или переменным. Если текущий слой переменный, то происходит вычисление потери по отношению к стилю для этого слоя. Для этого в функции используется грамм-матрица, которая вычисляется путем перемножения матрицы признаков (feature map) на себя транспонированную. Разница между грамм-матрицами `var_layer` и `const_layer` умножается на вес, чтобы получить потерю по отношению к стилю для текущего слоя.

Функция также производит изменение размера сегментов контента и стиля на каждом сверточном слое `CNN_structure`. Если текущий слой является сверточным слоем, то производится усреднение значений сегментов контента и стиля в каждом изображении. измененные значения сегментов контента и стиля используются при вычислении грамм-матриц. В конце функция возвращает список потерь по отношению к стилю для всех переменных слоев.

### Итоговые и Аффирные потери

```python
def total_variation_loss(output, weight):
    shape = output.get_shape()
    tv_loss = tf.reduce_sum(
        (output[:, :-1, :-1, :] - output[:, :-1, 1:, :]) * (output[:, :-1, :-1, :] - output[:, :-1, 1:, :]) +
        (output[:, :-1, :-1, :] - output[:, 1:, :-1, :]) * (output[:, :-1, :-1, :] - output[:, 1:, :-1, :])) / 2.0
    return tv_loss * weight

  
  

def affine_loss(output, M, weight):
    loss_affine = 0.0
    output_t = output / 255.
    for Vc in tf.unstack(output_t, axis=-1):
        Vc_ravel = tf.reshape(tf.transpose(Vc), [-1])
        loss_affine += tf.matmul(tf.expand_dims(Vc_ravel, 0),
                                 tf.sparse_tensor_dense_matmul(M, tf.expand_dims(Vc_ravel, -1)))
    return loss_affine * weight
```

Функция `total_variation_loss(output, weight)` вычисляет потерю общей вариации. Входные параметры – выход модели (`output`) и вес (`weight`). Функция использует tensorflow, чтобы вычислить разницу между пикселями изображения на соседних пикселях в вертикальном и горизонтальном направлении с помощью операций среза входных данных (`output[:, :-1, :-1, :]` и `output[:, :-1, 1:, :]`, также `output[:, 1:, :-1, :])`. Затем эти значения перемножаются и суммируются. Результат умножается на вес, чтобы получить общую потерю вариации.

Функция `affine_loss(output, M, weight)` вычисляет потерю аффинности. Входные параметры – выход модели (`output`), матрица преобразования (`M`) и вес (`weight`).
Функция использует `tensorflow`, чтобы произвести операцию разгона вывода в многомерный массив (`output_t`) и аффинное преобразование (`M`). Затем используется функция unstack из TensorFlow для извлечения последней оси (выходного изображения) и используем reshape для получения этой оси в виде одномерного массива. Это позволяет нам провести умножение между матрицами `M` и `output_t`. Каждое умножение содержится в отдельной матрице, которая затем суммируется, и результат умножается на вес, чтобы получить потерю аффинности.

## Запуск и эксплуатация

Программа запускается через консоль командой:

```powershall
python deep_photostyle.py --content_image_path ./examples/input/1.png --style_image_path ./examples/style/1.png --content_seg_path ./examples/segmentation/1.png --style_seg_path ./examples/segmentation/1_1.png --style_option 2 --serial ./test --save_iter 10
```

`--content_image_path` - место хранения изображения контента
`--style_image_path `- место хранения изображения стиля
`--content_seg_path` - место хранения изображения сегментации контента
`--style_seg_path` - место хранения изображения сегментации стиля
`--serial `- место сохранения изображения
`--save_iter` - через сколько эпох сохранять результат стилизации

Остальные настройки можно посмотреть в файле: `deep_photostyle.py`

## Результаты работы

Входное изображение: 

![Текст с описанием картинки](input/1.png)

Семантическая сегментация:
![img](segmentation/1.png)
Изображение стиля:

![img](style/1.png)
Семантическая сегментация
![img](segmentation/1_1.png)
Результат стилизации:
![img]()

Ещё несколько тестов представлены в папке `result`
